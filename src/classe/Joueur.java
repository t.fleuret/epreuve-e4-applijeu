package classe;

public class Joueur {
	private int id;
	private String pseudo;
	private String nationalite;
	private int age;
	
	 /**
    *
    * constructeur par default de jeu
    */
	public Joueur() {
		pseudo= null;
		nationalite=null;
		age = 18;
	}
	 /**
    *
    * constructeur de jeu
    */
	public Joueur(int id,String pseudo,String nationalite, int age) {
		this.id=id;
		this.pseudo= pseudo;
		this.nationalite=nationalite;
		this.age = age;
	}
	
	
	 public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	/**
    *
    * Permet d'obtenir le pseudo du joueur
    * @return - le pseudo du joueur sous forme de chaine de caract�res
    */
	public String getPseudo()  {
	   	 return pseudo;
	    }
	 /**
    *
    * Permet de modifier le pseudo d'un joueur
    */
	 public void setPseudo(String pseudo) {
	   	 this.pseudo = pseudo;
	    }
	 /**
	    *
	    * Permet d'obtenir le pseudo du joueur
	    * @return - la nationalite du joueur sous forme de chaine de caract�res
	    */
	 public String getnationalite()  {
	   	 return nationalite;
	    }
	 /**
	    *
	    * Permet de modifierla nationalite d'un joueur
	    */
	 public void setnationalite(String nationalite) {
	   	 this.pseudo = pseudo;
	    }
	 /**
	    * Permet d'obtenir l'age du joueur
	    * @return - l'age du joueur de type entier
	    */
public int getage()  {
  	 return age;
   }
/**
*
* Permet de modifier l'age d'un joueur
*/
public void setage(int age) {
  	 this.age = age;
   }

}

