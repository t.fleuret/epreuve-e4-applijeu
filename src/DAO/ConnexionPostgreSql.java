package DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnexionPostgreSql {
	//Git BDD Ok
	private static String url="jdbc:postgresql://172.16.0.57/t.fleuret";
	private static String user="t.fleuret";
	private static String passwd="P@ssword";	
	private static Connection connect;
	
	public static void Arreter() {
		try {
			connect.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static Connection getInstance(){
		if(connect == null){
		try {
		Class.forName("org.postgresql.Driver");
		connect = DriverManager.getConnection(url, user, passwd);
		} catch (SQLException e) {
		e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		}
		return connect;	
	}
}
